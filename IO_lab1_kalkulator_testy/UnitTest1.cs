using Microsoft.VisualStudio.TestTools.UnitTesting;
using IO_lab1_kalkulator;
using System;

namespace IO_lab1_kalkulator_testy
{
    [TestClass]
    public class UnitTest1
    {
 
        [TestMethod]
        public void Test_constant()
        {
            // Given
            string constant_string = "4";
            int expected_result = 4;


            // When
            int result = Kalkulator.Licz(constant_string);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void Test_emptystring()
        {
            // Given
            string empty_string = "";
            int expected_result = 0;


            // When
            int result = Kalkulator.Licz(empty_string);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void Test_addition_comma()
        {
            // Given
            string two_strings = "4,2";
            int expected_result = 6;


            // When
            int result = Kalkulator.Licz(two_strings);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void Test_addition_newline()
        {
            // Given
            string two_strings = "4\n2";
            int expected_result = 6;


            // When
            int result = Kalkulator.Licz(two_strings);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void Test_addition_multiple_inputs()
        {
            // Given
            string three_strings = "14\n4,3";
            int expected_result = 21;


            // When
            int result = Kalkulator.Licz(three_strings);


            // Then
            Assert.AreEqual(expected_result, result);

        }
        [TestMethod]
        public void Test_addition_negative_inputs()
        {
            // Given
            string three_strings = "-2,4,6";


            // When
            //int result = Kalkulator.Licz(three_strings);


            // Then
            Assert.ThrowsException<Exception>(() => Kalkulator.Licz(three_strings));

        }
        [TestMethod]
        public void Test_addition_big_input()
        {
            // Given
            string three_strings = "4,1235,8";
            int expected_result = 12;


            // When
            int result = Kalkulator.Licz(three_strings);


            // Then
            Assert.AreEqual(expected_result, result);
        }
        [TestMethod]
        public void Test_addition_big_input2()
        {
            // Given
            string three_strings = "1342,1235,5000";
            int expected_result = 0;


            // When
            int result = Kalkulator.Licz(three_strings);


            // Then
            Assert.AreEqual(expected_result, result);
        }
        [TestMethod]
        public void Test_new_separator()
        {
            // Given
            string new_separator = "//#\n4#5#6";
            int expected_result = 15;


            // When
            int result = Kalkulator.Licz(new_separator);


            // Then
            Assert.AreEqual(expected_result, result);
        }
        [TestMethod]
        public void Test_new_separator_multistring()
        {
            // Given
            string new_separator = "//[##]\n5##2##15";
            int expected_result = 22;


            // When
            int result = Kalkulator.Licz(new_separator);


            // Then
            Assert.AreEqual(expected_result, result);
        }
        [TestMethod]
        public void Test_new_separator_multistring_multi_separator()
        {
            // Given
            string new_separator = "//[##][,][???]\n14##3???7";
            int expected_result = 24;


            // When
            int result = Kalkulator.Licz(new_separator);


            // Then
            Assert.AreEqual(expected_result, result);
        }

        [TestMethod]
        public void Test_new_separator_multistring_multi_separator_another()
        {
            // Given
            string new_separator = "//[##][,][???][!!!]\n14##3???12";
            int expected_result = 29;


            // When
            int result = Kalkulator.Licz(new_separator);


            // Then
            Assert.AreEqual(expected_result, result);
        }
    }
}
